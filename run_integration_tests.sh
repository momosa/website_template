#!/bin/sh -ex

usage() {
cat << EOF
USAGE:

    --build as first arg to build the site.
    Remaining args are passed into the test command.

    Example:

        ./run_integration_tests.sh -- --tags "@test_env and not @purchase"

    See "Tag expressions" in: https://cucumber.io/docs/cucumber/api/#tags
EOF
}

#docker-compose up \
#    --exit-code-from \
#    test-runner

ORIGIN="${PWD}"
cd "$(dirname $0)"

if [ "${1}" = "--build" ]; then
    shift
    ./create_pages.sh
    ./sitemap.sh
    gatsby clean
    gatsby build
fi
gatsby serve &
GATSBY_PROD_SERVER=$!
TEST_DIR="$(dirname $0)/integration_tests"
TEST_SERVER=http://localhost:9000 \
    PATH="${PATH}:${TEST_DIR}" \
    npm --prefix "${TEST_DIR}" run test "$@" || \
    echo "Tests failed!" 1>&2
kill ${GATSBY_PROD_SERVER}
cd "${ORIGIN}"
