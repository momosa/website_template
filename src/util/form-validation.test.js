import getValidationErrors from "./form-validation"

const expectedNameErr = "Please provide a name that's at least 4 characters."
const expectedEmailErr = "Please provide a valid email address."
const expectedMessageErr = "Please provide a message between 20 and 1000 characters (about 140 words)."
const expectedPhoneErr = "Please provide a phone number of the form 123-456-7890."
const expectedGotchaErr = "Bad robot!"


/*******************************************************************************
 * Test Name validation
*******************************************************************************/

test("Form validation passes for normal names", () => {
    let data = new Map()
    data.set("name", "Foo bar the 2nd")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation yields an error for names that are too short", () => {
    let data = new Map()
    data.set("name", "Foo")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedNameErr)
});

test("Form validation yields an error for names that are too long", () => {
    let data = new Map()
    data.set("name", "Too Long".repeat(10))
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedNameErr)
});

/*******************************************************************************
 * Test email validation
*******************************************************************************/

test("Form validation passes for simple emails", () => {
    let data = new Map()
    data.set("email", "name123@domain.com")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for emails with a short extension", () => {
    let data = new Map()
    data.set("email", "name123@domain.eu")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for emails with a dash", () => {
    let data = new Map()
    data.set("email", "name-lastname@domain.com")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for emails with a .", () => {
    let data = new Map()
    data.set("email", "name.lastname@domain.com")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation raises an error for emails with no extension", () => {
    let data = new Map()
    data.set("email", "name.lastname@domain")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedEmailErr)
});

test("Form validation raises an error for emails with no @", () => {
    let data = new Map()
    data.set("email", "name.lastnamedomain.com")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedEmailErr)
});

test("Form validation raises an error for emails with no domain", () => {
    let data = new Map()
    data.set("email", "name.lastname@.com")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedEmailErr)
});

test("Form validation raises an error for emails with bad chars", () => {
    let data = new Map()
    data.set("email", "name!lastname@.com")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedEmailErr)
});

/*******************************************************************************
 * Test message validation
*******************************************************************************/

test("Form validation passes for normal messages", () => {
    let data = new Map()
    data.set("message", "This message is so informative")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for messages with newlines", () => {
    let data = new Map()
    data.set("message", "Testing testing 123\n\nHopefully this will work now")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation yields an error for messages that are too short", () => {
    let data = new Map()
    data.set("message", "Foo")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedMessageErr)
});

test("Form validation raises an error for messages that are too long", () => {
    let data = new Map()
    data.set("message", "Too Long".repeat(150))
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedMessageErr)
});

/*******************************************************************************
 * Test phone validation
*******************************************************************************/

test("Form validation passes for phone numbers with parentheses", () => {
    let data = new Map()
    data.set("phone", "(123) 456-7890")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for phone numbers with dashes", () => {
    let data = new Map()
    data.set("phone", "123-456-7890")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for phone numbers with just numbers", () => {
    let data = new Map()
    data.set("phone", "1234567890")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for phone numbers with a 1 prefix", () => {
    let data = new Map()
    data.set("phone", "1-123-456-7896")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for phone numbers with a +1 prefix", () => {
    let data = new Map()
    data.set("phone", "+1 123-456-7896")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation raises an error for phone numbers starting with letters", () => {
    let data = new Map()
    data.set("phone", "a23-456-7890")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedPhoneErr)
});

test("Form validation raises an error for phone numbers ending with letters", () => {
    let data = new Map()
    data.set("phone", "123-456-789a")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedPhoneErr)
});

test("Form validation raises an error for phone numbers with middle letters", () => {
    let data = new Map()
    data.set("phone", "123-45c-7890")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedPhoneErr)
});

test("Form validation raises an error for phone numbers that are too short", () => {
    let data = new Map()
    data.set("phone", "123456790")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedPhoneErr)
});

test("Form validation raises an error for phone numbers that are too long", () => {
    let data = new Map()
    data.set("phone", "123456789012")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedPhoneErr)
});

/*******************************************************************************
 * Test arbitrary required field validation
*******************************************************************************/

test("Form validation raises an error empty required fields", () => {
    let data = new Map()
    data.set("foo", "")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain("Foo is required.")
});

/*******************************************************************************
 * Test optional field validation
*******************************************************************************/

test("Form validation passes for empty optional field", () => {
    let data = new Map()
    data.set("phone", "")
    let optional = ["phone"]
    let errs = getValidationErrors(data, optional)
    expect(errs.length).toEqual(0)
});

test("Form validation passes for legit required and empty optional field", () => {
    let data = new Map()
    data.set("email", "foo@bar.com")
    data.set("phone", "")
    let optional = ["phone"]
    let errs = getValidationErrors(data, optional)
    expect(errs.length).toEqual(0)
});

test("Form validation raises an error illegit optional value", () => {
    let data = new Map()
    data.set("phone", "a23-456-7890")
    let optional = ["phone"]
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedPhoneErr)
});

/*******************************************************************************
 * Test honepot validation
*******************************************************************************/

test("Form validation passes for empty gotcha", () => {
    let data = new Map()
    data.set("_gotcha", "")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(0)
});

test("Form validation raises and error for non-empty gotcha", () => {
    let data = new Map()
    data.set("_gotcha", "I'm an evil robot")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(1)
    expect(errs).toContain(expectedGotchaErr)
});

/*******************************************************************************
 * Test multiple validations
*******************************************************************************/

test("Form validation yields an error for names and messages that are too short", () => {
    let data = new Map()
    data.set("name", "Foo")
    data.set("message", "Bar")
    let errs = getValidationErrors(data)
    expect(errs.length).toEqual(2)
    expect(errs).toContain(expectedNameErr)
    expect(errs).toContain(expectedMessageErr)
});

