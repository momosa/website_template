exports.Breakpoints = {
  largeOrLarger: `(min-width: 1440px)`,
  mediumOrLarger: `(min-width: 1024px)`,
  smallOrLarger: `(min-width: 768px)`,

  mediumOnly: `(min-width: 1024px) and (max-width: 1439px)`,
}
