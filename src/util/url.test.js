import URL from "./url"

/*******************************************************************************
 * Test CTOR
*******************************************************************************/

test("Constructor works for no params", () => {
    let url = new URL("https://www.foo.com/bar/baz")
    expect(url.fullPath).toEqual("https://www.foo.com/bar/baz")
    expect(url.protocol).toEqual("https")
    expect(url.domain).toEqual("www.foo.com")
    expect(url.relativePath).toEqual("/bar/baz")
    expect(url.hash).toEqual("")
    expect(url.params.size).toEqual(0)
});

test("Constructor works for no params and hash", () => {
    let url = new URL("https://www.foo.com/bar/baz#hash")
    expect(url.fullPath).toEqual("https://www.foo.com/bar/baz")
    expect(url.protocol).toEqual("https")
    expect(url.domain).toEqual("www.foo.com")
    expect(url.relativePath).toEqual("/bar/baz")
    expect(url.hash).toEqual("#hash")
    expect(url.params.size).toEqual(0)
});

test("Constructor works for one param", () => {
    let url = new URL("https://www.foo.com?key=value")
    expect(url.fullPath).toEqual("https://www.foo.com")
    expect(url.protocol).toEqual("https")
    expect(url.domain).toEqual("www.foo.com")
    expect(url.relativePath).toEqual("/")
    expect(url.hash).toEqual("")
    expect(url.params.size).toEqual(1)
    expect(url.params.get("key")).toEqual("value")
});

test("Constructor works for multiple params", () => {
    let url = new URL("https://www.foo.com?firstKey=firstValue&secondKey=secondValue")
    expect(url.fullPath).toEqual("https://www.foo.com")
    expect(url.protocol).toEqual("https")
    expect(url.domain).toEqual("www.foo.com")
    expect(url.relativePath).toEqual("/")
    expect(url.hash).toEqual("")
    expect(url.params.size).toEqual(2)
    expect(url.params.get("firstKey")).toEqual("firstValue")
    expect(url.params.get("secondKey")).toEqual("secondValue")
});

test("Constructor works for multiple params and hash", () => {
    let url = new URL("https://www.foo.com?firstKey=firstValue&secondKey=secondValue#hash")
    expect(url.fullPath).toEqual("https://www.foo.com")
    expect(url.protocol).toEqual("https")
    expect(url.domain).toEqual("www.foo.com")
    expect(url.relativePath).toEqual("/")
    expect(url.hash).toEqual("#hash")
    expect(url.params.size).toEqual(2)
    expect(url.params.get("firstKey")).toEqual("firstValue")
    expect(url.params.get("secondKey")).toEqual("secondValue")
});

test("Constructor works for no-value params", () => {
    let url = new URL("https://www.foo.com?firstKey&secondKey=secondValue")
    expect(url.fullPath).toEqual("https://www.foo.com")
    expect(url.protocol).toEqual("https")
    expect(url.domain).toEqual("www.foo.com")
    expect(url.relativePath).toEqual("/")
    expect(url.hash).toEqual("")
    expect(url.params.size).toEqual(2)
    expect(url.params.get("firstKey")).toEqual(undefined)
    expect(url.params.get("secondKey")).toEqual("secondValue")
});

/*******************************************************************************
 * Test getParamStr
*******************************************************************************/

test("Getting a param string with no params", () => {
    let url = new URL("https://www.foo.com")
    expect(url.getParamStr()).toEqual("")
});

test("Getting a param string with one param", () => {
    let url = new URL("https://www.foo.com?key=value")
    expect(url.getParamStr()).toEqual("?key=value")
});

test("Getting a param string with multiple params", () => {
    let url = new URL("https://www.foo.com?firstKey=firstValue&secondKey=secondValue")
    expect(url.getParamStr()).toEqual("?firstKey=firstValue&secondKey=secondValue")
});

test("Getting a param string with no-value params", () => {
    let url = new URL("https://www.foo.com?firstKey&secondKey=secondValue")
    expect(url.getParamStr()).toEqual("?firstKey&secondKey=secondValue")
});

/*******************************************************************************
 * Test getFull
*******************************************************************************/

test("Getting the full URL with no params", () => {
    let url = new URL("https://www.foo.com")
    expect(url.getFull()).toEqual("https://www.foo.com")
});

test("Getting the full URL with no params and hash", () => {
    let url = new URL("https://www.foo.com#hash")
    expect(url.getFull()).toEqual("https://www.foo.com#hash")
});

test("Getting the full URL with one param", () => {
    let url = new URL("https://www.foo.com?key=value")
    expect(url.getFull()).toEqual("https://www.foo.com?key=value")
});

test("Getting the full URL with multiple params", () => {
    let url = new URL("https://www.foo.com?firstKey=firstValue&secondKey=secondValue")
    expect(url.getFull()).toEqual("https://www.foo.com?firstKey=firstValue&secondKey=secondValue")
});

test("Getting the full URL with multiple params and hash", () => {
    let url = new URL("https://www.foo.com?firstKey=firstValue&secondKey=secondValue#hash")
    expect(url.getFull()).toEqual("https://www.foo.com?firstKey=firstValue&secondKey=secondValue#hash")
});

test("Getting the full URL with no-value params", () => {
    let url = new URL("https://www.foo.com?firstKey&secondKey=secondValue")
    expect(url.getFull()).toEqual("https://www.foo.com?firstKey&secondKey=secondValue")
});
