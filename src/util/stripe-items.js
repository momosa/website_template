/* Tools for stripe
 *
 * stripeItem(data)
 *
 */

// Get streamlined data from stripe price node
exports.stripeItem = node => {
  const data = {
    id: node.product.id,
    name: node.product.name,
    description: node.product.description,
    keywords: [],
    images: [],
    image: undefined,
    priceId: node.id,
    sku: node.sku,
    price: node.unit_amount,
    currency: node.currency,
    path: node.fields.path,
  }

  if (node.product.metadata.keywords) {
    for (const keyword of node.product.metadata.keywords.split(",")) {
      data.keywords.push(keyword.trim())
    }
  }

  if (node.product.localFiles) {
    data.image = node.product.localFiles[0].publicURL
    for (const fileNode of node.product.localFiles) {
      data.images.push(`${fileNode.name}${fileNode.ext}`)
    }
  }
  
  return data
}
