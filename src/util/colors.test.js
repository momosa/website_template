import RGB from "./colors"

/*******************************************************************************
 * Test CTOR
*******************************************************************************/

test("Constructor works for integers", () => {
  const color = new RGB(1, 2, 3)
  expect(color.red).toEqual(1)
  expect(color.green).toEqual(2)
  expect(color.blue).toEqual(3)
})

test("Constructor works for floats", () => {
  const color = new RGB(1.1, 2.2, 3.8)
  expect(color.red).toEqual(1)
  expect(color.green).toEqual(2)
  expect(color.blue).toEqual(3)
})

/*******************************************************************************
 * Test copy
*******************************************************************************/

test("Copy creates a new object", () => {
  const origColor = new RGB(1, 2, 3)
  const copyColor = origColor.copy()
  expect(origColor).not.toBe(copyColor)
})

test("Copied colors are the same color as the original", () => {
  const origColor = new RGB(1, 2, 3)
  const copyColor = origColor.copy()
  expect(copyColor.red).toEqual(origColor.red)
  expect(copyColor.green).toEqual(origColor.green)
  expect(copyColor.blue).toEqual(origColor.blue)
})

/*******************************************************************************
 * Test value
*******************************************************************************/

test("Value works for white", () => {
  const color = new RGB(255, 255, 255)
  expect(color.value()).toBeCloseTo(1.0)
})

test("Value works for black", () => {
  const color = new RGB(0, 0, 0)
  expect(color.value()).toBeCloseTo(0.0)
})

test("Value works for intermediate", () => {
  const color = new RGB(111, 222, 123)
  expect(color.value()).toBeCloseTo(0.62718131460055715293)
})

test("Value works for some zero", () => {
  const color = new RGB(111, 0, 222)
  expect(color.value()).toBeCloseTo(0.56196228944970362647)
})

/*******************************************************************************
 * Test scaled
*******************************************************************************/

test("Scaled creates a new object", () => {
  const origColor = new RGB(1, 2, 3)
  const scaledColor = origColor.scaled()
  expect(origColor).not.toBe(scaledColor)
})

test("Scaled maps white to black", () => {
  const color = new RGB(255, 255, 255),
    scaledColor = color.scaled(0)
  expect(scaledColor.value()).toBeCloseTo(0)
  expect(scaledColor.red).toEqual(0)
  expect(scaledColor.green).toEqual(0)
  expect(scaledColor.blue).toEqual(0)
})

test("Scaled maps black to white", () => {
  const color = new RGB(0, 0, 0),
    scaledColor = color.scaled(1)
  expect(scaledColor.value()).toBeCloseTo(1.0)
  expect(scaledColor.red).toEqual(255)
  expect(scaledColor.green).toEqual(255)
  expect(scaledColor.blue).toEqual(255)
})

test("Scaled maps black to white", () => {
  const color = new RGB(0, 0, 0),
    scaledColor = color.scaled(1)
  expect(scaledColor.value()).toBeCloseTo(1.0)
  expect(scaledColor.red).toEqual(255)
  expect(scaledColor.green).toEqual(255)
  expect(scaledColor.blue).toEqual(255)
})

test("Scaled creates a shorter vector given a mix of 255 and 0", () => {
  const color = new RGB(255, 0, 255),
    scaledColor = color.scaled(0.5)
  expect(scaledColor.value()).toBeCloseTo(0.5)
  expect(scaledColor.red).toEqual(156)
  expect(scaledColor.green).toEqual(0)
  expect(scaledColor.blue).toEqual(156)
})

test("Scaled selects the nearest color given a mix of 255 and 0", () => {
  const color = new RGB(255, 0, 255),
    scaledColor = color.scaled(0.9)
  expect(scaledColor.value()).toBeCloseTo(0.9)
  expect(scaledColor.red).toEqual(255)
  expect(scaledColor.green).toEqual(167)
  expect(scaledColor.blue).toEqual(255)
})

test("Scaled creates a shorter vector given intermediate RGB values", () => {
  const color = new RGB(64, 224, 208),
    scaledColor = color.scaled(0.5)
  expect(scaledColor.value()).toBeCloseTo(0.5)
  expect(scaledColor.red).toEqual(45)
  expect(scaledColor.green).toEqual(158)
  expect(scaledColor.blue).toEqual(147)
})

test("Scaled selects the nearest color given intermediate RGB values", () => {
  const color = new RGB(64, 224, 208),
    scaledColor = color.scaled(0.9)
  expect(scaledColor.value()).toBeCloseTo(0.9)
  expect(scaledColor.red).toEqual(167)
  expect(scaledColor.green).toEqual(255)
  expect(scaledColor.blue).toEqual(255)
})

/*******************************************************************************
 * Test mixed
*******************************************************************************/

test("Mixed combines black and white gray", () => {
  const black = new RGB(0, 0, 0),
    white = new RGB(255, 255, 255),
    gray = black.mixed(white)
  expect(gray.red).toEqual(127)
  expect(gray.green).toEqual(127)
  expect(gray.blue).toEqual(127)
})

test("Mixed combines black and white to lightgray", () => {
  const black = new RGB(0, 0, 0),
    white = new RGB(255, 255, 255),
    lightgray = black.mixed(white, 0.75)
  expect(lightgray.red).toEqual(191)
  expect(lightgray.green).toEqual(191)
  expect(lightgray.blue).toEqual(191)
})

test("Mixed combines black and white to darkgray", () => {
  const black = new RGB(0, 0, 0),
    white = new RGB(255, 255, 255),
    darkgray = black.mixed(white, 0.25)
  expect(darkgray.red).toEqual(63)
  expect(darkgray.green).toEqual(63)
  expect(darkgray.blue).toEqual(63)
})

test("Mixed combines red and gray", () => {
  const gray = new RGB(127, 127, 127),
    red = new RGB(255, 0, 0),
    mixedColor = red.mixed(gray)
  expect(mixedColor.red).toEqual(191)
  expect(mixedColor.green).toEqual(63)
  expect(mixedColor.blue).toEqual(63)
})

test("Mixed with a negative ratio combines gray and black to lightgray", () => {
  const black = new RGB(0, 0, 0),
    gray = new RGB(127, 127, 127),
    lightgray = gray.mixed(black, -0.5)
  expect(lightgray.red).toEqual(190)
  expect(lightgray.green).toEqual(190)
  expect(lightgray.blue).toEqual(190)
})

/*******************************************************************************
 * Test inverted
*******************************************************************************/

test("Inverted white is black", () => {
  const white = new RGB(255, 255, 255),
    invertedColor = white.inverted()
  expect(invertedColor.red).toEqual(0)
  expect(invertedColor.green).toEqual(0)
  expect(invertedColor.blue).toEqual(0)
})

test("Inverted red is cyan", () => {
  const red = new RGB(255, 0, 0),
    invertedColor = red.inverted()
  expect(invertedColor.red).toEqual(0)
  expect(invertedColor.green).toEqual(255)
  expect(invertedColor.blue).toEqual(255)
})

test("Inverted aquamarine is correct", () => {
  const aquamarine = new RGB(127, 255, 212),
    invertedColor = aquamarine.inverted()
  expect(invertedColor.red).toEqual(128)
  expect(invertedColor.green).toEqual(0)
  expect(invertedColor.blue).toEqual(43)
})

/*******************************************************************************
 * Test toString
*******************************************************************************/

test("Red toString is correct", () => {
  const red = new RGB(255, 0, 0),
    colorString = red.toString()
  expect(colorString).toEqual("rgb(255,0,0)")
})

test("Aquamarine toString is correct", () => {
  const aquamarine = new RGB(127, 255, 212),
    colorString = aquamarine.toString()
  expect(colorString).toEqual("rgb(127,255,212)")
})

/*******************************************************************************
 * Test toArray
*******************************************************************************/

test("Red toArray is correct", () => {
  const red = new RGB(255, 0, 0),
    arr = red.toArray()
  expect(arr[0]).toEqual(255)
  expect(arr[1]).toEqual(0)
  expect(arr[2]).toEqual(0)
})

test("Aquamarine toArray is correct", () => {
  const aquamarine = new RGB(127, 255, 212),
    arr = aquamarine.toArray()
  expect(arr[0]).toEqual(127)
  expect(arr[1]).toEqual(255)
  expect(arr[2]).toEqual(212)
})
