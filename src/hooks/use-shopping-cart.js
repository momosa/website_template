/* useShoppingCart
 */

import { useContext } from "react"
import CartContext from "../util/cart-context"

export default function useShoppingCart() {
  return useContext(CartContext)
}
