/* Video
 *
 * Standard HTML video
 * https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video
 *
 * For future improvements, see:
 * https://www.gatsbyjs.com/docs/how-to/images-and-media/working-with-video/
 */

import React from "react"
import styled from "styled-components"

const StyledVideo = styled.video`
  display: block;
  margin: auto;
  margin-bottom: 0px;
  border: 0;
  width: min(90%, 600px);
`

export default function Video(props) {
  if (!props.src) {
    throw ReferenceError("prop 'src' not defined")
  }
  return (
    <StyledVideo
      controls
      data-qa={props["data-qa"] || "Video"}
    >
      <source
        src={props.src}
        type={props.type || "video/mp4"}
      />
    </StyledVideo>
  )
}

