/* LocalProducts
 *
 *  Fetches data and passes it to the products component
 *
 *  Props:
 *    purchase (defaults to global setting): Boolean for including purchase actions
 *      If using local products and selling via stripe, product IDs must match Local
 *    filter (optional): function taking in a product (map) and returning a Boolean
 *    sort (optional): compare function for sorting as described in
 *      https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
 *      Defaults to lexicographic ordering by name.
 *
 *  Examples:
 *    <LocalProducts />
 *    <LocalProducts
 *      purchase={true}
 *      filter={product => product.keywords.includes("entree")}
 *      sort={(p1, p2) => p1.price < p2.price ? 1 : -1}
 *    />
 */

import React from "react"
import useLocalItems from "../hooks/use-local-items"

import Products from "./products"

export default function LocalProducts(props) {

  const items = useLocalItems()

  return (
    <Products
      purchase={props.purchase}
      filter={props.filter}
      sort={props.sort}
      items={items}
      data-qa={props["data-qa"] || "LocalProducts"}
    />
  )
}
