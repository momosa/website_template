/* NoveltyBullets
 * Props:
 *   icon: the fontawesome icon string, like "fas fa-paw"
 *   content: bullet points content in an array
 *     Assumes text only (because of key)
 *   columns (optional): number of columns for large screens, default is 2
 *   width (optional): width of the list on large screens, default is "50%"
 *
 * Usage:
 *
 *   <NoveltyBullets
 *     icon={"fas fa-paw"}
 *     content={[
 *       "Bullet points",
 *       "Custom icons",
 *       "Cuban sandwiches",
 *       "Maths",
 *     ]}
 *   />
 *
 *   <NoveltyBullets
 *     icon={"fas fa-paw"}
 *     content={[
 *       "These list items have longer content",
 *       "So it's better to use only one column and increase the width",
 *     ]}
 *     columns={1}
 *     width={"80%"}
 *   />
 */

import React from "react"
import styled from "styled-components"
import { Breakpoints } from "../util/breakpoints"

const StyledBullets = styled.div`
  display: inline-grid;
  width: 100%;
  grid-template-columns: auto;
  grid-row-gap: 12px;
  grid-column-gap: 36px;

  @media ${Breakpoints.smallOrLarger} {
    grid-template-columns: repeat(${props => props.columns}, auto);
  }
`

const StyledItem = styled.div`
  display: grid;
  grid-template-columns: min-content auto;
  grid-column-gap: 12px;
  color: var(--linkColor);
  font-size: large;
  font-weight: bold;
  align-items: start;
`

const Centered = styled.div`
  margin-left: 12px;

  @media ${Breakpoints.smallOrLarger} {
    margin: auto;
    width: ${props => props.width};
  }
`

export default function NoveltyBullets(props) {

  const contentItems = props.content.map( contentItem =>
    <StyledItem key={contentItem}>
      <i class={props.icon}></i>
      {contentItem}
    </StyledItem>
  )

  return (
    <Centered width={props.width}>
      <StyledBullets
        columns={props.columns}
        data-qa={props["data-qa"] || "NoveltyBullets"}
      >
        {contentItems}
      </StyledBullets>
    </Centered>
  )
}

NoveltyBullets.defaultProps = {
  columns: 2,
  width: "50%",
}
