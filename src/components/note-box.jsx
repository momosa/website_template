import React from "react"
import styled from "styled-components"

const StyledNoteBox = styled.div`
  display: grid;
  border-left: solid gray 1px;
  grid-template-columns: auto-fit;
  grid-column-gap: 0;
  grid-row-gap: 1em;
  margin: 1em;
  padding: 1em;
  word-wrap: anywhere;
`

const NoteHeader = styled.div`
  font-size: large;
  text-transform: uppercase;
  letter-spacing: 0.2em;
  background-color: var(--darkBackgroundColor);
  padding-left: 6px;
  padding-top: 6px;
  padding-bottom: 6px;
`

export default function NoteBox(props) {
  return (
    <StyledNoteBox data-qa={props["data-qa"] || "NoteBox"}>
      <NoteHeader data-qa={"NoteBoxHeader"}>
        {props.type}
      </NoteHeader>
      <div data-qa={"NoteBoxContent"}>
        {props.content}
      </div>
    </StyledNoteBox>
  )
}
