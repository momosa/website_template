/* User defined data
 *
 * Input user data here.
 * Ensure each item is defined, at least as an empty string.
 */
import React from "react"
import styled, { keyframes } from "styled-components"
import { Breakpoints } from "./src/util/breakpoints"
import useSiteMetadata from "./src/hooks/use-site-metadata"
import useUserPreferences from "./src/hooks/use-user-preferences"

import TabLink from "./src/components/tab-link"
import ModalLink from "./src/components/modal-link"
import Carousel from "./src/components/carousel"
import SlideShow from "./src/components/slideshow"
import DynamicImage from "./src/components/dynamic-image"
import CustomForm from "./src/components/custom-form"
import NoteBox from "./src/components/note-box"
import DisplayItemList from "./src/components/display-item-list"
import TableItemList from "./src/components/table-item-list"
import GoogleMap from "./src/components/google-map"
import Youtube from "./src/components/youtube"
import GoogleCalendar from "./src/components/google-calendar"

import Modal from "./src/components/modal"
import ItemDetails from "./src/components/item-details"

import LocalProducts from "./src/components/local-products"

import TabList from "./src/components/tab-list"
import Title from "./src/components/title"

/**********************************************************************
 * Input client website customization below
 **********************************************************************/

const footer = (<>
  <p>
    &copy; 2021&nbsp;
    <a href="https://opensocialresources.com/" target="_blank" rel="noreferrer">
      Open Social Resources
    </a>. All rights reserved.
    Fontawesome icons are licensed under&nbsp;
    <a href="https://fontawesome.com/license" target="_blank" rel="noreferrer">
      CC-BY 4.0</a>. 
  </p>
</>)

/* Tab info and settings
 * Any tabs you want.
 *
 * For special tabs and components, you'll need to adjust imports in this file
 *
 * Use ProgressiveImage for images over img
 */

const aboutTab = {
  slug: "about",
  name: "About",
  content: (<>
    <h2>About us</h2>
    <p>
      This is a sample website for the
      Website Template Project open source repository.
      If you'd like us to create a website for you,
      visit us at&nbsp;
      <a href="https://website-template.opensocialresources.com/"
        target="_blank" rel="noreferrer"
      >website-template.opensocialresources.com</a>.
    </p>
    <p>
      The Website Template Project is designed to help small businesses
      create professional websites.
      The project is developed and maintained by&nbsp;
      <a href="https://opensocialresources.com/"
        target="_blank" rel="noreferrer">Open Social Resources</a>.
      To support our non-profit and this project, please visit us at&nbsp;
      <a href="https://opensocialresources.com/donate/" target="_blank" rel="noreferrer">
        opensocialresources.com/donate
      </a>.
    </p>
  </>),
}

const generalTab = {
  slug: "general",
  name: "General",
  content: (<>
    <h2>General features</h2>
    <p>
      Here are some general purpose features.
      See the user content file for technical details.
    </p>

    <h3>Carousel</h3>
    <p>
      Carousels are great for groups of images,
      such as images for a single product.
      Click on the active image to enlarge.
    </p>
    <div style={{width: "min(100%, 600px)"}}>
      <Carousel
        images={["robots.jpg", "coctail.jpg", "goldengate.jpg"]}
      />
    </div>

    <h3>Slideshow</h3>
    <p>
      Slideshows are useful for passively showcasing images on a timer.
      Spash images are automatically built into a slideshow.
    </p>
    <div style={{width: "min(100%, 600px)"}}>
      <SlideShow
        images={["robots.jpg", "coctail.jpg", "goldengate.jpg"]}
      />
    </div>

    <h3>Notebox</h3>
    <NoteBox type="note" content={(<div>
      This is a template with sample content for the open source repo.
      If you want to know about us, visit us at:
      <p><a href="https://website-template.opensocialresources.com/"
        target="_blank" rel="noreferrer">
        website-template.opensocialresources.com/</a></p>
    </div>)} />

    <h3>Modals</h3>
    <p>
      You can put almost anything into a modal.
    </p>
    {/* Create a modal */}
    <Modal id="item-details-sample" contentLabel="Item Details Sample" scrolly="maybe">
      <ItemDetails
        title="Hint of food"
        src="tall.jpg"
        longDescription={(<>
          <h3>Description</h3>
          <p>
            You can put as much info as you need to here.
            The modal will scroll if necessary.
          </p>
        </>)}
      />
    </Modal>
    <p>
      You can create a link that will bring up your modal content like this:&nbsp;
      <ModalLink modalId="item-details-sample">Sample item details</ModalLink>.
    </p>

    <h3>Tables</h3>
    <p>
      Sometimes items are best represented in a table.
      This can help users to scan for information, like prices.
      Tables are also great to list written documents.
      Remember that a picture says a thousand words,
      so only use tables to capitalize on their visual presenatation.
    </p>
    <TableItemList
      title={"Guest Services"}
      header={["Service", "Price", "Description"]}
      items={[
        ["Adult haircut", "$20", "Premium haircut for guests 18 or older"],
        ["Kids haircut", "$15", "Premium haircut for guests under 18"],
        ["Facial waxing", "$10", "Let your face glow with our professional waxing"],
      ]}
    />

    <h3>TabLinks</h3>
    <p>
      You can link to another tab like this:&nbsp;
      <TabLink to="/items">items</TabLink>.
    </p>
  
  </>),
}

const itemsTab = {
  slug: "items",
  name: "Items",
  content: (<>
    <h2>Items</h2>
    <p>
      Whether or not you're selling your items online,
      you can showcase them on your website.
    </p>

    <h3>Local products</h3>
    <p>
      You can define products in local files. See&nbsp;
      <a href="https://gitlab.com/momosa/website_template/-/tree/master/content/items"
        target="_blank" rel="noreferrer">here</a> for
      examples defining local products.
    </p>
    {/* Purchasing will only work for local items if the id matches Stripe */}
    {/* To minimize sources of truth, only use StripeProducts for purchasable items */}
    <LocalProducts purchase={false} />
    <p>You can also filter items, like appetizers:</p>
    <LocalProducts
      filter={product => product.keywords.includes("appetizer")}
      purchase={false}
    />

    <h3>Stripe products</h3>
    <p>
      If you have e-commerce set up with Stripe,
      you can create a product list of your Stripe items.
      You can get item data from Stripe whether or not
      users can purchase items through the site.
      See the&nbsp;
      <a href="https://gitlab.com/momosa/website_template/-/blob/master/README.md"
        target="_blank" rel="noreferrer">README</a> for
      instructions on getting items from stripe.
    </p>
    {/*
    <StripeProducts />
    <p>You can filter Stripe products:</p>
    <StripeProducts
      filter={product => product.keywords.includes("entree")}
      purchase={false}
    />
    */}

    <h3>Inline items</h3>
    <p>
      You can use the same display as an item list,
      but with any content.
      You can optionally attach modals with more details to each item.
      This can be handy for bio pics or other content.
    </p>
  
    <DisplayItemList items={new Map([
  
      ["Robots", new Map(Object.entries({
        primaryInfo: (<>
          {/* The price of a menu item or a person's title for a bio */}
          Two cute robots
        </>), description: (<>
          {/* A description of the item */}
          <i>Click on the image or title to see details.</i>
        </>), image: "robots.jpg",
        // The details attribute, if provided, will attach a modal
        details: new Map(Object.entries({
          itemId: "robots",
          detailsContent: (<>
            <h3>About the robots</h3>
            <p>
              You can put whatever you want here.
              The modal will scroll if necessary.
            </p>
            <p>
              You can even link to other modals on the same tab.
            </p>
            <p>
              If you don't include details content,
              The primary info and description will be used by default.
            </p>
            <p>
              Notice that here and in the display item list,
              strongly proportioned images will have a white
              box so they still look good.
            </p>
          </>)
        }))
      }))],
  
    ])} />
  

  </>),
}

const formsTab = {
  slug: "forms",
  name: "Forms",
  content: (<>
    <h2>Forms</h2>
    <p>
      Because of the potential for abuse,
      it's better to use a contact form service over just placing your email.
      You can easily create custom forms like this:
    </p>

    <CustomForm
      debug={true}
      id="my-form"
      action={"https://formspree.io/your-endpoint"}
      fields={[
        new Map(Object.entries({
          fieldName: "name",
          required: true,
          label: "Name",
          input: "text",
        })),
        new Map(Object.entries({
          fieldName: "email",
          required: true,
          label: "Email",
          input: "email",
        })),
        new Map(Object.entries({
          fieldName: "phone",
          required: false,
          label: "Phone number",
          input: "tel",
        })),
        new Map(Object.entries({
          fieldName: "message",
          required: true,
          label: "Message",
          input: "textarea",
        })),
      ]}
      formFields={["name", "email", "message"]}
    />
  </>),
}

const externalTab = {
  slug: "external",
  name: "External",
  content: (<>
    <h2>External Data</h2>
    <p>
      You can embed items from external sites,
      like maps or videos.
    </p>

    <h3>YouTube</h3>
    <Youtube src="https://www.youtube.com/embed/6you0V2sb-4" />

    <h3>Google Maps</h3>
    <GoogleMap src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2949.5134055941053!2d-83.09662308416101!3d42.331576279188894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883b32ba305433a1%3A0x70da09958baf802c!2sMike&#39;s%20Famous%20Ham%20Place!5e0!3m2!1sen!2sus!4v1600166816699!5m2!1sen!2sus" />

    <h3>Google Calendars</h3>
    <GoogleCalendar src="https://calendar.google.com/calendar/embed?wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FNew_York&amp;src=ZW4udXNhI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%230B8043&amp;title=Holidays%20in%20the%20United%20States&amp;showNav=1&amp;showTabs=1" />

  </>),
}

const tabs = [
  aboutTab,
  generalTab,
  itemsTab,
  formsTab,
  externalTab,
]

/*******************************************************************************
 *
 * Business Splash
 *
 * Defined here to handle a lot of customization.
 * Consider relocating to components once we have themes.
 *
 ******************************************************************************/

const Separator = styled.hr`
  border-top: 1px solid lightgray;
`

// Need to move the id marker up so navigation is visible
// Use only if bottom tab list present
const TabContentPlaceholder = styled.div`
  position: relative;
  top: -3em;
`
const StyledBusinessIdentifiers = styled.div`
  width: 100%;
  display: inline-grid;
  box-sizing: border-box;
  grid-template-columns: ${props => props.shouldUseIconLogo ?
    'min-content auto' :
    'auto'
  };
  column-gap: 15px;
  justify-content: start;
  align-items: center;
  grid-template-areas: ${props => props.shouldUseIconLogo ?
    '"icon title" "description description"' :
    '"title" "description"'
  };

  @media ${Breakpoints.smallOrLarger} {
    padding: 15px;
    padding-top: 0;
    row-gap: 7px;
    grid-template-columns: min-content auto min-content;
    grid-template-areas:
      "icon        title"
      "icon        description";
  }
`

const TitleContainer = styled.div`
  min-width: 60vw;
  grid-area: title;
`

const IconContainer = styled.div`
  grid-area: icon;
  height: 70px;
  width: 70px;

  @media ${Breakpoints.smallOrLarger} {
    height: 120px;
    width: 120px;
    padding-right: 15px;
    margin-bottom: 8px;
  }
`

const SlideshowContainer = styled.div`
  background-color: var(--backgroundColor);
  @media ${Breakpoints.smallOrLarger} {
    margin: auto;
    max-width: 90%;
  }
  @media ${Breakpoints.largeOrLarger} {
    max-width: 80%;
  }
`

const slideLeft = keyframes`
  from {
    margin-left: 100%;
    width: 300%;
  }
  to {
    margin-left: 0%;
    width: 100%;
  }
`

const DescriptionContainer = styled.p`
  grid-area: description;
  font-style: italic;
  font-size: larger;
  animation: ${slideLeft} 1s ease-out;

  @media ${Breakpoints.smallOrLarger} {
    margin: 0;
    vertical-align: middle;
  }
`

function BusinessIdentifiers(props) {
  const siteMetadata = useSiteMetadata()
  return (
    <StyledBusinessIdentifiers
      shouldUseIconLogo={false}
      data-qa={props["data-qa"] || "BusinessIdentifiers"}
    >

      <IconContainer data-qa={"BusinessIcon"}>
        <DynamicImage
          src="icon.png"
          alt="Business icon"
          data-qa="BusinessIcon"
          shouldShowBackground={false}
        />
      </IconContainer>

      <TitleContainer>
      {/*
        <h1>
          <DynamicImage
            src="banner_logo.png"
            alt={siteMetadata.title}
            data-qa="BusinessBanner"
            shouldShowBackground={false}
          />
        </h1>
      */}
        <Title
          text={siteMetadata.title}
          data-qa={"BusinessTitle"}
        />
      </TitleContainer>

      <DescriptionContainer data-qa={"BusinessDescription"}>
        {siteMetadata.description}
      </DescriptionContainer>

    </StyledBusinessIdentifiers>

  )
}

function TabPageSplash(props) {
  const userPreferences = useUserPreferences()

  return (<>
    <div role={"navigation"}>
      <TabList
        activeTab={props.activeTab}
        tabs={tabs}
      />
    </div>

    <BusinessIdentifiers />
    <SlideshowContainer>
      <SlideShow
        images={userPreferences.splashImages}
        data-qa={"SplashImages"}
      />
    </SlideshowContainer>


    {/* Ensure bottom tab list is visible when switching tabs
        Only use with bottom tab list
    */}
    <TabContentPlaceholder id="TabContent" />

    <TabList
      activeTab={props.activeTab}
      tabs={tabs}
    />

    <Separator />
  </>)
}

export default function getUserContent() {
  return {
    footer,
    tabs,
    BusinessIdentifiers,
    TabPageSplash,
  }
}
