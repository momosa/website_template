# Test server dockerfile

FROM node

RUN mkdir -p /test-server
COPY . /test-server
WORKDIR /test-server
RUN npm install
RUN npm install -g gatsby-cli
RUN ./create_pages.sh
RUN ./sitemap.sh
RUN gatsby build

CMD gatsby serve --host 0.0.0.0
