# Changelog

## 2.0: Features for site types

* **1.4** E-commerce products (in-progress)
  - Improve splash area design
  - Fix: item page on mobile extends beyond viewport when there are many images
  - Fix: background not darker when modal is open
  - Make the splash section more customizable

* **1.3** Arts and crafts
  - Automatically adjust contrast ratios for custom colors to meet W3C AAA criteria
  - Integrate carousels into item pages
  - Create image carousel

* **1.2** Investments
  - Add automated integration tests
  - Add embedded Google Calendars
  - Make tab navigation more accessible
  - Add a home button to the header
  - Use item pages instead of item modals
  - Simplify image components
  - Update Gatsby and plugins
  - Make the splash image and business description optional
  - Optimize the shopping cart
  - Simplify user preferences and content specification
  - Modernize React code
  - Enable splashing with a slideshow instead of a single image
  - Enable an ultra-performant slideshow
  - Use single-file components

* **1.1** E-commerce
  - Improve item detail presentation
  - Improve item detail SEO
  - Make business logos/icons optional
  - Enable use of banner (long) logos for the business title
  - Showcase items from Stripe (with and without purchase options)
  - Enable checkout via Stripe
  - Enable a shopping cart
  - Improve social sharing interface
  - Enable page/item-specific social sharing

## 1.0: MVP

* **1.0** Single effective template
  - Improve the design
  - Improve SEO
  - Enhance accessibility
  - Enhance performance
  - Improve the styling
  - Improve automated testing
  - Simplify color specification
  - Make user customization safer
* **0.1** Project kickoff
  - Search Engine Optimization (SEO)
  - Enable Formspree contact forms
  - Enable social sharing
  - Optimize image loading for slow connections
  - Streamline customization
  - Create a basic layout for mobile and desktop
