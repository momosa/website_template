#!/bin/sh -ex

usage() {
cat << EOF
USAGE:

    --build as first arg to build the site.
    Remaining args are passed into the test command.

    Examples:

        ./run_integration_tests.sh -- --tags "@test_env and not @purchase"
        ./run_integration_tests.sh features/general.feature

    See "Tag expressions" in: https://cucumber.io/docs/cucumber/api/#tags
EOF
}

#docker-compose up \
#    --exit-code-from \
#    test-runner

ORIGIN="${PWD}"
cd "$(dirname ${0})"
INTEGRATION_TEST_DIR="${PWD}"

AVAILABLE_SETTINGS="TEST_SERVER HEADLESS TIMEOUT"
if [ -f ./.settings ]; then
    for setting in $(cat ./.settings); do
        field=${setting%%=*}
        echo ${AVAILABLE_SETTINGS} | grep ${field} 2>&1 > /dev/null || {
            echo "${field} is not a recognized setting" 1>&2
            exit 1
        }
        # Only use the settings file if the value isn't already set
        # E.g., the value in TIMEOUT=10000 ./run_integration_tests.sh should take priority
        eval old_value=\$${field}
        if [ -n "${field}" -a -z "${old_value}" ]; then
            value=${setting#*=}
            eval export ${field}="${value}"
        fi
    done
fi

if [ -z "${TEST_SERVER}" ]; then
    export TEST_SERVER="https://website-template-project.gitlab.io/private-repo"
fi

LOCAL_SERVER=""
if echo "${TEST_SERVER}" | grep "localhost" 2>&1 > /dev/null; then
    LOCAL_SERVER=yes
fi

cd ..

if [ -n "${LOCAL_SERVER}" ]; then
    if [ "${1}" = "--build" ]; then
        shift
        ./create_pages.sh
        ./sitemap.sh
        gatsby clean
        gatsby build
    fi
    gatsby serve &
    GATSBY_PROD_SERVER=$!
    echo Sleeping for 5 seconds to ensure the server is ready for tests
    sleep 5
fi

PATH="${PATH}:${INTEGRATION_TEST_DIR}" \
    npm --prefix "${INTEGRATION_TEST_DIR}" run test "$@" || \
    echo "Tests failed!" 1>&2

if [ -n "${LOCAL_SERVER}" -a -n "${GATSBY_PROD_SERVER}" ]; then
    kill ${GATSBY_PROD_SERVER}
fi

cd "${ORIGIN}"
