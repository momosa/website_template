# Integration testing

## Running integration tests

```shell
cd [PROJECT_DIR]/integration_tests
./run_integration_tests.sh

# Run tests from a specific feature file
./run_integration_tests.sh features/general.feature

# Run tests passing args to cucumber
# Note the extra --
./run_integration_tests.sh -- --tags "@wip"

# Run tests with settings
# You can also set you settings in integration_tests/.settings
HEADLESS=true TIMEOUT=10000 TEST_SERVER=http://localhost:9000 ./run_integration_tests.sh
```

## Settings

You can pass in settings as mentioned above or
by creating `integration_tests/.settings`.
Sample settings file:

```shell
HEADLESS=true
TIMEOUT=10000
TEST_SERVER=http://localhost:9000
```

## The script

The script `run_integration_tests.sh`
should help streamline the testing process.

1. The script will only use values from the `./settings` file if
  those values aren't already set.
  E.g., the value in `TIMEOUT=10000 ./run_integration_tests.sh`
  will take priority over any `TIMEOUT` value in the settings file.
1. The script will default to the remote test server if
  `TEST_SERVER` isn't set.
1. If you use `TEST_SERVER=http://localhost:9000`,
  the script will automatically start and stop the test server.
  See the project README for how to set up your local environment.
