# features/forms.feature

@test_env
Feature: Testing Features on the /forms/ page
  Background:
    Given I visit the "/forms/" page

  Scenario: The form sends the correct data without the optional phone number
    When I enter "Super Cat" in the "name" input
    And I enter "supercat@gmail.com" in the "email" input
    And I enter "Super Cat loves messages." in the "message" input
    And I click the "SubmitButton"
    Then the form sends "name: Super Cat"
    And the form sends "email: supercat@gmail.com"
    And the form sends "Super Cat loves messages."

  Scenario: The form sends the correct data with the optional phone number
    When I enter "Super Cat" in the "name" input
    And I enter "697-873-7228" in the "phone" input
    And I enter "supercat@gmail.com" in the "email" input
    And I enter "Super Cat loves messages." in the "message" input
    And I click the "SubmitButton"
    Then the form sends "name: Super Cat"
    And the form sends "phone: 697-873-7228"
    And the form sends "email: supercat@gmail.com"
    And the form sends "Super Cat loves messages."

  Scenario: The form sends the correct data with the optional phone number
    When I enter "Bad" in the "name" input
    And I enter "Bad" in the "phone" input
    And I enter "Bad" in the "email" input
    And I enter "Bad" in the "message" input
    And I click the "SubmitButton"
    Then there is a "ValidationError" that says "Please provide a name that's at least 4 characters."
    And there is a "ValidationError" that says "Please provide a valid email address."
    And there is a "ValidationError" that says "Please provide a phone number of the form 123-456-7890."
    And there is a "ValidationError" that says "Please provide a message between 20 and 1000 characters (about 140 words)."
