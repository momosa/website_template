// features/support/world.js
const {
  setWorldConstructor,
  World,
  setDefaultTimeout
} = require("@cucumber/cucumber")
const { Builder } = require("selenium-webdriver")
const firefox = require("selenium-webdriver/firefox")

const SCREEN = {
  width: 640,
  height: 480,
}

const DEFAULT_TIMEOUT = !!process.env.TIMEOUT ? parseInt(process.env.TIMEOUT) : 5 * 1000

class CustomWorld extends World {
  driver = process.env.HEADLESS === "true" ? 
    new Builder().forBrowser("firefox")
      .setFirefoxOptions(new firefox.Options().headless().windowSize(SCREEN))
      .build() :
    new Builder().forBrowser("firefox").build()

  constructor(options) {
    super(options)
    this.siteUrl = process.env.TEST_SERVER || "http://localhost:9000"
    this.defaultTimeout = DEFAULT_TIMEOUT
  }

}

setWorldConstructor(CustomWorld)
setDefaultTimeout(DEFAULT_TIMEOUT)
