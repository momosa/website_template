# features/general.feature

@test_env
Feature: Testing Features on the /general/ page
  Background:
    Given I visit the "/general/" page
  
  Scenario: Links to tabs work
    When I click the "items" "TabLink"
    Then I am on the "/items#TabContent" page

  Scenario: The modal close button works
    When I click the "Sample item details" "ModalLink"
    And I click the "CloseModalButton"
    Then the modal is closed

  Scenario: The modal close button works
    When I click the "Sample item details" "ModalLink"
    And I press the "ESCAPE" key
    Then the modal is closed

  Scenario: Clicking outside of the modal closes it
    When I click the "Sample item details" "ModalLink"
    And I click outside of the modal
    Then the modal is closed
