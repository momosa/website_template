# features/homepage.feature

Feature: Testing Features on the Homepage
  Background:
    Given I visit the "/" page
  
  @smoke
  Scenario: The home button works
    When I click the "HomeButton"
    Then I am on the "/" page

  @purchase
  Scenario: The cart button works 
    When I click the "CartButton"
    Then I am on the "/cart/" page

  @test_env 
  Scenario Outline: The tab navigation works
    When I click the "<navigatorText>" "TabNavigator"
    Then I am on the "<path>" page

    Examples:
      | navigatorText | path                 |
      | General       | /general#TabContent  |
      | Items         | /items#TabContent    |
      | Forms         | /forms#TabContent    |
      | External      | /external#TabContent |

  
  Scenario: The back to top button works
    When I click the "BackToTopButton"
    Then the "HomeButton" is selected

