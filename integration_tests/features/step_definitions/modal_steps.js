const { When, Then } = require("@cucumber/cucumber")
const { Origin } = require("selenium-webdriver")
const assert = require("assert").strict
const { findQa } = require("./util")


When(`I click outside of the modal`, async function () {
  await this.driver.actions()
    .move({origin: Origin.VIEWPORT, x: 0, y: 0})
    .click()
    .perform()
})


Then(`the modal is closed`, async function () {
  const overlay = await findQa(this.driver, "Overlay")
  const display = await overlay.getCssValue("display")
  assert.equal(display, "none")
})
