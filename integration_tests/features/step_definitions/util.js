/* features/support/util.js
 *
 * findQa(element or driver, string): element
 * findAllQa(element or driver, string): array of elements
 * findXpath(element or driver, string): element
 * findAllXpath(element or driver, string): array of elements
 *
 * Usage:
 *
 *   const button = await findQa(driver, "HomeButton")
 *   const link = await findXpath(driver, `//*[@data-qa="TabLink" and text()="items"]`
 */

const { By, until } = require("selenium-webdriver")

/* findQa
 *
 * Params:
 *   element: web element or driver
 *   dataQa: string representing the data-qa attribute of the element
 *
 * Returns: web element
 */
exports.findQa = async (element, dataQa) =>
  await element.wait(until.elementLocated(
    By.css(`[data-qa="${dataQa}"]`)
  ))

/* findAllQa
 *
 * Params:
 *   element: web element or driver
 *   dataQa: string representing the data-qa attribute of the elements
 *
 * Returns: array of web elements
 */
exports.findAllQa = async (element, dataQa) =>
  await element.wait(until.elementsLocated(
    By.css(`[data-qa="${dataQa}"]`)
  ))

/* findXpath
 *
 * Params:
 *   element: web element or driver
 *   xpath: string representing the xpath of the element
 *
 * Returns: web element
 */
exports.findXpath = async (element, xpath) =>
  await element.wait(until.elementLocated(
    By.xpath(xpath)
  ))

/* findAllXpath
 *
 * Params:
 *   element: web element or driver
 *   xpath: string representing the xpath of the elements
 *
 * Returns: array of web elements
 */
exports.findAllXpath = async (element, dataQa) =>
  await element.wait(until.elementsLocated(
    By.xpath(xpath)
  ))
