const { When, Then } = require("@cucumber/cucumber")
const { until } = require("selenium-webdriver")
const assert = require("assert").strict
const { findXpath } = require("./util")


/* Enters text into a form field
 * E.g., I enter "Super Cat" in the "name" input
 */
When(`I enter {string} in the {string} input`, async function (inputText, inputName) {
  const inputElement = await findXpath(
    this.driver,
    `//*[@data-qa="FormInput" and @name="${inputName}"]`,
  )
  this.driver.wait(until.elementIsVisible(inputElement))
  await inputElement.sendKeys(inputText)
})


/* Ensure that the form sends a message in debug mode
 * E.g., the form sends "name: Super Cat"
 *
 * In debug mode, the would-be message is sent to an alert.
 *
 * https://www.selenium.dev/documentation/en/webdriver/js_alerts_prompts_and_confirmations/
 */
Then(`the form sends {string}`, async function (formString) {
  await this.driver.wait(until.alertIsPresent())
  const formAlert = await this.driver.switchTo().alert()
  const alertText = await formAlert.getText()
  assert(
    alertText.includes(formString),
    `Did not find "${formString}" in:\n${alertText}`,
  )
})

