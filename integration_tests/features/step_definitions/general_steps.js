const { Given, When, Then } = require("@cucumber/cucumber")
const { until, Key } = require("selenium-webdriver")
const assert = require("assert").strict
const { findQa, findXpath } = require("./util")


/* E.g., I visit the "/general/" page
 */
Given(`I visit the {string} page`, async function (page) {
  const url = `${this.siteUrl}${page}`
  // Visit the page
  await this.driver.get(url)
  await this.driver.wait(until.urlIs(url))
})


/* Click on an element identified by a data-qa attribute
 * E.g., I click the "SubmitButton"
 */
When(`I click the {string}`, async function (elementDataQa) {
  const matchingElement = await findQa(this.driver, elementDataQa)
  // Ensure the button is clickable
  this.driver.wait(until.elementIsVisible(matchingElement))
  await matchingElement.click()
})


/* Click on an element identified by text and a data-qa attribute
 * E.g., I click the "Sample item details" "ModalLink"
 */
When(`I click the {string} {string}`, async function (elementText, elementDataQa) {
  const matchingElement = await findXpath(
    this.driver,
    `//*[@data-qa="${elementDataQa}" and text()="${elementText}"]`,
  )
  // Ensure element is clickable
  this.driver.wait(until.elementIsVisible(matchingElement))
  // Click it
  await matchingElement.click()
})


/* Press a special key like "ESCAPE" or "ENTER"
 * E.g., I press the "ESCAPE" key
 *
 * For actions and sendKeys: https://www.selenium.dev/selenium/docs/api/javascript/module/selenium-webdriver/lib/input_exports_Actions.html
 *
 * For a list of keys: https://www.selenium.dev/selenium/docs/api/javascript/module/selenium-webdriver/lib/input_exports_Key.html#SHIFT
 */
When(`I press the {string} key`, async function (keyCode) {
  await this.driver.actions()
    .sendKeys(Key[keyCode])
    .perform()
})


/* Verify the current page
 * E.g., I am on the "/items#TabContent" page
 */
Then(`I am on the {string} page`, async function (expectedPage) {
  // Wait to be on the correct page
  // That's enough to validate, but the failure output lacks the actual URL
  // Ensure informative output from assert
  const expectedUrl = `${this.siteUrl}${expectedPage}`
  try {
    await this.driver.wait(until.urlIs(expectedUrl), this.defaultTimeout - 1000)
  } finally {
    const actualUrl = await this.driver.getCurrentUrl()
    assert.equal(actualUrl, expectedUrl)
  }
})


/* Verify that an element is selected, identified by a data-qa attribute
 * E.g., the "HomeButton" is selected
 */
Then(`the {string} is selected`, async function (elementDataQa) {
  const element = await findQa(this.driver, elementDataQa)
  // Ensure that element is selected
  this.driver.wait(until.elementIsSelected(element))
})


/* Ensure an element is present, identified by data-qa and text
 *
 * E.g., there is a "ValidationError" that says "Please provide a valid email address."
 */
Then(`there is a {string} that says {string}`, async function (elementDataQa, elementText) {
  const matchingElement = await findXpath(
    this.driver,
    `//*[@data-qa="${elementDataQa}" and text()="${elementText}"]`,
  )
  this.driver.wait(until.elementIsVisible(matchingElement))
})
