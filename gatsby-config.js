/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

// .env files are required for Stripe. See the README
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const Breakpoints = require("./src/util/breakpoints").Breakpoints

module.exports = {
  flags: {
    DEV_SSR: true,
  },

  /* Your site config here */

  // Use a pathPrefix if serving under a path,
  // like your_project for username.gitlab.io/your_project
  pathPrefix:`/website_template`,

  siteMetadata: {
    title: `Your Business`,
    owner: `Your Name`,
    siteUrl: `https://momosa.gitlab.io/website_template/`,
    // The description should have at most 160 chars (including spaces)
    description: `A brief description of your business`,
    twitterUsername: ``,
    // Search Engine Optimization (SEO)
    // Keywords aren't commonly used, but good to check with your content
    keywords: [
      `some keywords`,
      `characterizing`,
      `your business`,
    ],

    userPreferences: {
      // Place one or more splash images in src/images/
      splashImages: ["main_image.jpg"],

      language: "en-US",

      // E-commerce
      purchase: false,
      currency: "USD",

      /* Main color used in the design
       * Choose based on hue and saturation, not value in the color-theoretic sense
       * Named colors: https://www.rapidtables.com/web/css/css-color.html
       * Specify using RGB values.
       */
      primaryThemeColor: [62, 134, 210],
      /* Background color
       * Actual background colors are derived from the given color
       * Expect fairly light versions
       * Specify using RGB values
       */
      backgroundColor: [242, 242, 242],
    },
  },


  plugins: [

    // Manifest for site info and progressive web apps
    // https://www.gatsbyjs.com/plugins/gatsby-plugin-manifest/
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Your Business`,
        short_name: `Your Business`,
        start_url: `/`,
        background_color: `#f2f2f2`,
        theme_color: `#77ff77`,
        display: `standalone`,
        icon: `src/images/icon.png`,
        icons: [
          { // Designate the icon file used for SEO
            src: `/icon.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
        icon_options: {
          purpose: `any maskable`,
        },
      },
    },

    //{ // Stripe for e-commerce sites
    //  // https://www.gatsbyjs.com/plugins/gatsby-source-stripe/
    //  resolve: `gatsby-source-stripe`,
    //  options: {
    //    objects: ["Price", "Product"],
    //    secretKey: process.env.STRIPE_SECRET_KEY,
    //    downloadFiles: true,
    //  },
    //},

    /************************************************************
     * Don't touch below unless doing development.
     * The following settings should apply to all sites.
     ************************************************************/

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/`,
      },
    },

    // For item data and other user content
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `content`,
        path: `${__dirname}/content/`,
      },
    },

    // https://www.gatsbyjs.com/plugins/gatsby-plugin-breakpoints/
    {
      resolve: "gatsby-plugin-breakpoints",
      options: {
        queries: Breakpoints,
      },
    },

    // https://www.gatsbyjs.com/plugins/gatsby-transformer-yaml/
    `gatsby-transformer-yaml`,

    `gatsby-plugin-react-helmet`,

    // https://www.gatsbyjs.com/plugins/gatsby-plugin-image/
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-sharp`,
      options: {
        useMozJpeg: true,
      },
    },

    // Styled components for CSS-in-JS
    // https://www.gatsbyjs.com/docs/how-to/styling/styled-components/
    // https://styled-components.com/docs
    `gatsby-plugin-styled-components`,

    {
      // https://www.gatsbyjs.com/plugins/gatsby-plugin-csp/
      resolve: `gatsby-plugin-csp`,
      options: {
        disableOnDev: true,
        // Changes header to Content-Security-Policy-Report-Only for csp testing purposes
        reportOnly: false,
        // you can disable scripts sha256 hashes
        mergeScriptHashes: true,
        // you can disable styles sha256 hashes
        // Needs to be false. See https://github.com/bejamas/gatsby-plugin-csp/issues/3#issuecomment-521032340
        mergeStyleHashes: false,
        mergeDefaultDirectives: true,
        directives: {
          "default-src": [
            "'self'",
            // Allow talking to google for maps
            "www.google.com",
            // Allow talking to youtube for videos
            "https://www.youtube.com",
            // Allow talking to stripe for product info and e-commerce
            "https://js.stripe.com",
          ].join(" "),
          "script-src": [
            "'self'",
            // Allow eval for modals
            "'unsafe-eval'",
            // Allow talking to stripe for product info and e-commerce
            "https://js.stripe.com",
          ].join(" "),
          "style-src": [
            "'self'",
            // Required for some inline styles, like the hidden _gotcha form field
            "'unsafe-inline'",
            // Allow getting fontawesome icons
            "https://use.fontawesome.com",
          ].join(" "),
          // Allow getting fontawesome icons
          "font-src": "'self' https://use.fontawesome.com",
          // Allow talking to formspree for contact and custom forms
          "connect-src": "'self' https://formspree.io",
        }
      },
    },

    // Enables offline mode for better perf and web-app functionality
    // https://www.gatsbyjs.com/plugins/gatsby-plugin-offline/
    // To remove: https://www.gatsbyjs.com/plugins/gatsby-plugin-offline/#remove
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: [`/`],
      },
    },

  ],
}
